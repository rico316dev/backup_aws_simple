#!/bin/bash

# Defina as variáveis do script
S3_BUCKET="seu-bucket-s3"
BACKUP_DIR="/caminho/do/diretorio/que/sera/backupado"
TODAY=$(date +%Y-%m-%d)
YESTERDAY=$(date -d "yesterday" +%Y-%m-%d)
LAST_BACKUP=$(aws s3 ls s3://$S3_BUCKET | awk '{print $4}' | grep ^$TODAY | tail -n 1)

# Verifique se já foi feito backup hoje
if [[ $LAST_BACKUP == "" ]]; then
  # Realize um backup completo se não houver backup hoje
  aws s3 sync $BACKUP_DIR s3://$S3_BUCKET/$TODAY --delete
else
  # Realize um backup incremental se já houver backup hoje
  aws s3 sync $BACKUP_DIR s3://$S3_BUCKET/$TODAY --delete --exclude "*" --include "*$YESTERDAY*" --include "*/*.log"
fi